
export interface StateProps {
  title: string
}

export interface DispatchProps {
  dispatch?(): void
}
