import { CpuTemperatureData, CpuLoad } from './Cpu'

export interface CpuFormatBase {
  current: number,
  min: number,
  max: number,
}

export interface CpuFormat {
  main: CpuFormatBase,
  cores: Array<CpuFormatBase>
}

export class CpuFormats {
  static formatCpuTemp (cpuTemp: CpuTemperatureData): CpuFormat {
    const cpuFormat: CpuFormat = {
      main: {
        current: cpuTemp.main,
        min: Infinity,
        max: 0
      },
      cores: cpuTemp.cores.map((item) => {
        return {
          current: item,
          min: Infinity,
          max: 0
        }
      })
    }
    return cpuFormat
  }

  static formatCpuLoad (cpuLoad: CpuLoad): CpuFormat {
    const cpuFormat: CpuFormat = {
      main: {
        current: cpuLoad.currentload,
        min: Infinity,
        max: 0
      },
      cores: cpuLoad.cpus.map((item) => {
        return {
          current: item.load,
          min: Infinity,
          max: 0
        }
      })
    }
    return cpuFormat
  }

  static compareValues (currentCores: Array<CpuFormatBase>, newCores: Array<CpuFormatBase>): Array<CpuFormatBase> {
    const newList: Array<CpuFormatBase> = currentCores.map((item, index) => {
      return {
        current: newCores[index].current,
        min: newCores[index].current < item.min ? newCores[index].current : item.min,
        max: newCores[index].current > item.max ? newCores[index].current : item.max
      }
    })
    return [...newList]
  }
}
