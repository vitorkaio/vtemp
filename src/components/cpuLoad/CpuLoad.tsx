import React from 'react'
import { StateProps, DispatchProps } from './CpuLoadTypes'

import { Container, Content, Main, AvgTemp, MinMaxTemp, Cores, Core } from './CpuLoadStyles'
import ProgressCpu from '../progressCpu/ProgressCpu'
import { CpuFormat } from '../../services/CpuFormats'
import CalculateLoad from './CalculateLoad'

import { cpuTempGood, cpuTempMin, cpuTempWarning, cpuTempCritical } from '../../styles/Colors'
import { MyBadge, MyCore } from '../badges/Badges'
import Header from '../header/Header'
import Tooltip from '@material-ui/core/Tooltip'
import { CPUTooltip } from '../tooltips/Tooltips'

type Props = DispatchProps & StateProps

const CpuLoad: React.FC<Props> = ({ cpuLoad }) => {
  const newLoad: CpuFormat = CalculateLoad.newLoads(CalculateLoad.setup(cpuLoad))

  const color = (load: number) => {
    const value = load
    if (value <= 20) {
      return cpuTempMin
    } else if (value > 20 && value <= 50) {
      return cpuTempGood
    } else if (value > 50 && value <= 90) {
      return cpuTempWarning
    } else {
      return cpuTempCritical
    }
  }

  return (
    <Container>
      <Header title="CPU Load" />
      <Content>
        <Main>
          <AvgTemp>
            <Tooltip title={CPUTooltip.CPU_LOAD_CURRENT} aria-label="info">
              <div>
                <ProgressCpu
                  value={parseInt(newLoad?.main.current.toFixed())}
                  text={`${newLoad?.main.current.toFixed()}%`}
                  color={color(parseInt(newLoad?.main.current.toFixed()))}
                  size={150} />
              </div>
            </Tooltip>
          </AvgTemp>
          <MinMaxTemp>
            <Tooltip title={CPUTooltip.CPU_LOAD_MIN} aria-label="info">
              <div>
                <ProgressCpu
                  value={parseInt(newLoad?.main.min.toFixed())}
                  text={`${newLoad?.main.min.toFixed()}%`}
                  color={cpuTempMin}
                  size={90} />
              </div>
            </Tooltip>
            <Tooltip title={CPUTooltip.CPU_LOAD_MAX} aria-label="info">
              <div>
                <ProgressCpu
                  value={parseInt(newLoad?.main.max.toFixed())}
                  text={`${newLoad?.main.max.toFixed()}%`}
                  color={cpuTempCritical}
                  size={90} />
              </div>
            </Tooltip>

          </MinMaxTemp>
        </Main>
        <Cores>
          {
            newLoad.cores.map((item, index) => (
              <Core key={index}>
                <MyCore><span>Core #{++index}</span></MyCore>
                <Tooltip title={CPUTooltip.CORE_LOAD_CURRENT} aria-label="info">
                  <MyBadge color={color(parseInt(item.current.toFixed()))}><span>{item.current.toFixed()}</span></MyBadge>
                </Tooltip>
                <Tooltip title={CPUTooltip.CORE_LOAD_MIN} aria-label="info">
                  <MyBadge color={cpuTempMin}><span>{item.min.toFixed()}</span></MyBadge>
                </Tooltip>
                <Tooltip title={CPUTooltip.CORE_LOAD_MAX} aria-label="info">
                  <MyBadge color={cpuTempCritical}><span>{item.max.toFixed()}</span></MyBadge>
                </Tooltip>
              </Core>
            ))
          }
        </Cores>
      </Content>
    </Container>
  )
}

export default CpuLoad
