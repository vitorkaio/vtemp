import styled from 'styled-components'
import { primaryColor } from '../../styles/Colors'

interface Props {
  color: string
}
export const MyBadge = styled.div<Props>`
  width: 30px;
  height: 30px;
  border: 1px solid transparent;
  border-radius: 50%;
  padding: 0.1rem;
  background-color: ${(props: Props) => (props.color)};

  display: flex;
  justify-content: center;
  align-items: center;

  color: white;
  font-family: "JetBrains Mono Medium";
  font-size: 0.8rem;
  font-weight: bold;
`

export const MyCore = styled.div`
  border: 1px solid transparent;
  border-radius: 0.8rem;
  padding: 0.3rem 0.5rem;
  background-color: ${primaryColor[500]};

  display: flex;
  justify-content: center;
  align-items: center;

  color: white;
  font-family: "JetBrains Mono Medium";
  font-size: 0.8rem;
  font-weight: bold;
`
