
export enum CPUTooltip {
  CPU_TEMP_CURRENT = 'CPU Current Temperature',
  CPU_TEMP_MIN = 'CPU Min Temperature',
  CPU_TEMP_MAX = 'CPU Max Temperature',

  CORE_TEMP_CURRENT = 'Core Current Temperature',
  CORE_TEMP_MIN = 'Core Min Temperature',
  CORE_TEMP_MAX = 'Core Max Temperature',

  CPU_LOAD_CURRENT = 'CPU Current Load',
  CPU_LOAD_MIN = 'CPU Min Load',
  CPU_LOAD_MAX = 'CPU Max Load',

  CORE_LOAD_CURRENT = 'Core Current Load',
  CORE_LOAD_MIN = 'Core Min Load',
  CORE_LOAD_MAX = 'Core Max Load',
}
