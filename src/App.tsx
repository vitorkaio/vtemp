import React from 'react'
import { render } from 'react-dom'
import { GlobalStyle } from './styles/GlobalStyle'
import { ThemeProvider } from '@material-ui/core/styles'
import MyTheme from './styles/MyTheme'

// import Greetings from './components/Greetings'
import Home from './pages/home/Home'

const mainElement = document.createElement('div')
mainElement.setAttribute('id', 'root')
document.body.appendChild(mainElement)

const App = () => {
  return (
    <ThemeProvider theme={MyTheme}>
      <GlobalStyle />
      <Home />
    </ThemeProvider>
  )
}

render(<App />, mainElement)
