import styled from 'styled-components'

interface Props {
  size: number
}

export const Container = styled.div<Props>`
  width: ${(props: Props) => (`${props.size}px`)};
  height: ${(props: Props) => (`${props.size}px`)};
`
