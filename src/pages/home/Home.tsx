import React, { useEffect, useState } from 'react'
import { Cpu, CpuInfos } from '../../services/Cpu'

import { Container, Loading, LoadingTitle } from './HomeStyles'
import CpuTemp from '../../components/cpuTemp/CpuTemp'
import CpuLoad from '../../components/cpuLoad/CpuLoad'
import CpuInfo from '../../components/cpuInfo/CpuInfo'
import Author from '../../components/author/Author'

import CircularProgress from '@material-ui/core/CircularProgress'

let intervalId = 0

const Home: React.FC = () => {
  const [cpuInfos, setCpuInfos] = useState<CpuInfos>()

  // catch info from cpu
  const verifyCpu = async () => {
    const infos = await Cpu.cpuInfo()
    setCpuInfos(infos)
  }

  useEffect(() => {
    intervalId = setInterval(() => {
      verifyCpu()
    }, 1e3)
  }, [])

  // componentWillUnmount
  useEffect(() => {
    return () => {
      clearInterval(intervalId)
    }
  }, [])

  // console.log(cpuInfos)

  return (
    <Container>
      {
        cpuInfos
          ? (
            <>
              <CpuTemp cpuTemp={cpuInfos.temp} />
              <CpuLoad cpuLoad={cpuInfos.load} />
              <CpuInfo cpuData={cpuInfos?.info} />
            </>
          )
          : (
            <Loading>
              <CircularProgress size={80} />
              <LoadingTitle>Carregando Dados</LoadingTitle>
            </Loading>
          )
      }
      <Author />
    </Container>
  )
}

export default Home
