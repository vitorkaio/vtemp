import { CpuTemperatureData } from '../../services/Cpu'

export interface StateProps {
  cpuTemp: CpuTemperatureData
}

export interface DispatchProps {
  dispatch?(): void
}
