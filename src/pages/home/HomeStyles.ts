import styled from 'styled-components'
import { primaryColor } from '../../styles/Colors'

export const Container = styled.div`
  width: 100%;
  height: 100vh;

  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`

export const Loading = styled.div`
  flex: 1;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  & > span {
    margin-top: 0.7rem;
  }

`

export const LoadingTitle = styled.span`
  font-family: "JetBrains Mono Medium";
  font-weight: bold;
  font-size: 1.3rem;
  color: ${primaryColor[500]}
`
