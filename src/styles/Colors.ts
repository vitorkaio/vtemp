import { purple, orange } from '@material-ui/core/colors'

export const primaryColor = purple
export const secondaryColor = '#303030'
export const archivesColor = orange[700]

export const cpuTempGood = '#42b983'
export const cpuTempMin = 'cornflowerblue'
export const cpuTempWarning = '#fc6d26'
export const cpuTempCritical = '#f00'
