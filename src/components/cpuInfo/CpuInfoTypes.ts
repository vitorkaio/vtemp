import { CpuData } from '../../services/Cpu'

export interface StateProps {
  cpuData: CpuData | undefined
}

export interface DispatchProps {
  dispatch?(): void
}
