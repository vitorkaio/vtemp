import { createMuiTheme } from '@material-ui/core/styles'
import { primaryColor, secondaryColor } from './Colors'

const theme = createMuiTheme({
  palette: {
    primary: primaryColor,
    secondary: {
      main: secondaryColor
    }
  }
})

export default theme
