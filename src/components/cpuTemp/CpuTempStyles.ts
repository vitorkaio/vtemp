import styled from 'styled-components'

export const Container = styled.div`
  flex: 1;
  width: 100%;
  
  display: flex;
  flex-direction: column;
  min-height: 230px;
`
export const Content = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
`

export const Main = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
`

export const AvgTemp = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`
export const MinMaxTemp = styled.div`
  flex: 1;
  display: flex;
  justify-content: space-around;
  align-items: center;
`

export const Cores = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  overflow-y: auto;
  max-height: 200px;
`

export const Core = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin: 0.6rem;
`
