// Closure que calcula os novos valores: current, min e max

import { CpuFormat, CpuFormats } from '../../services/CpuFormats'
import { CpuTemperatureData } from 'services/Cpu'

const CalculateTemp = () => {
  let temp = <CpuFormat>{}

  const setup = (cpuTemp: CpuTemperatureData): CpuFormat => {
    return CpuFormats.formatCpuTemp(cpuTemp)
  }

  const newTemps = (newData: CpuFormat): CpuFormat => {
    if (Object.keys(temp).length !== 0) {
      temp.main.current = newData.main.current
      temp.main.min = newData.main.current < temp.main.min ? newData.main.current : temp.main.min
      temp.main.max = newData.main.current > temp.main.max ? newData.main.current : temp.main.max
      temp.cores = CpuFormats.compareValues([...temp.cores], [...newData.cores])
    } else {
      temp = newData
    }

    return temp
  }

  const getTemp = (): CpuFormat => temp

  return {
    getTemp,
    setup,
    newTemps
  }
}

export default CalculateTemp()

/* draft.main.current = newData.main.current
draft.main.min = newData.main.current < temp.main.min ? newData.main.current : temp.main.min
draft.main.max = newData.main.current > temp.main.max ? newData.main.current : temp.main.max
draft.cores = CpuFormats.compareValues([...temp.cores], [...newData.cores]) */
