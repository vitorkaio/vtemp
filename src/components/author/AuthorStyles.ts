import styled from 'styled-components'

export const Container = styled.a`
  position: absolute;
  bottom: 2px;
  right: 10px;

  display: flex;
  justify-content: center;
  align-items: center;
`

export const AuthorTitle = styled.span`
  font-size: 0.6rem;
  font-weight: 600;
`
