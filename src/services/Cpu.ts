import sys, { Systeminformation } from 'systeminformation'

export type CpuData = Systeminformation.CpuData
export type CpuTemperatureData = Systeminformation.CpuTemperatureData
export type CpuLoad = Systeminformation.CurrentLoadData
export type ProcessesData = Systeminformation.ProcessesData
export interface CpuInfos {
  info: CpuData,
  temp: CpuTemperatureData,
  load: CpuLoad,
  process: ProcessesData
}

export class Cpu {
  /**
   * Retorna as informações da cpu, tal como: nome, tipo, cache, cores...
   *
   * @static
   * @memberof Cpu
   */
  static async cpuData (): Promise<CpuData> {
    try {
      const cores = await sys.cpu()
      return cores
    } catch (err) {
      throw Error(err)
    }
  }

  /**
   * Informações sobre a temperatura da cpu.
   *
   * @static
   * @memberof Cpu
   */
  static async cpuTemp (): Promise<CpuTemperatureData> {
    try {
      const cpuTemperature = await sys.cpuTemperature()
      return cpuTemperature
    } catch (err) {
      throw Error(err)
    }
  }

  /**
   * Mescla as inforamações sobre a cpu e sua temperatura.
   *
   * @static
   * @memberof Cpu
   */
  static async cpuInfo (): Promise<CpuInfos> {
    try {
      const cpuTemperature = await sys.cpuTemperature()
      const cpuData = await sys.cpu()
      const cpuLoad = await sys.currentLoad()
      const cpuProcess = await sys.processes()
      const cpuInfo: CpuInfos = {
        info: cpuData,
        temp: cpuTemperature,
        load: cpuLoad,
        process: cpuProcess
      }
      return cpuInfo
    } catch (err) {
      throw Error(err)
    }
  }
} // Fim da classe
