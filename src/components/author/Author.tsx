import React from 'react'

import { Container, AuthorTitle } from './AuthorStyles'
import GitlabIcon from '../../assets/gitlab.svg'

const Author: React.FC = () => {
  return (
    <Container href='https://gitlab.com/vitorkaio/vtemp' target="_blank">
      <img src={GitlabIcon} alt='gitlab' width='25px' height='25px' />
      <AuthorTitle>vitorkaio</AuthorTitle>
    </Container>
  )
}

export default Author
