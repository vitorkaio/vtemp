// Closure que calcula os novos valores: current, min e max

import { CpuFormat, CpuFormats } from '../../services/CpuFormats'
import { CpuLoad } from 'services/Cpu'

const CalculateLoad = () => {
  let load = <CpuFormat>{}

  const setup = (cpuLoad: CpuLoad): CpuFormat => {
    return CpuFormats.formatCpuLoad(cpuLoad)
  }

  const newLoads = (newData: CpuFormat): CpuFormat => {
    if (Object.keys(load).length !== 0) {
      load.main.current = newData.main.current
      load.main.min = newData.main.current < load.main.min ? newData.main.current : load.main.min
      load.main.max = newData.main.current > load.main.max ? newData.main.current : load.main.max
      load.cores = CpuFormats.compareValues([...load.cores], [...newData.cores])
    } else {
      load = newData
    }

    return load
  }

  const getLoad = (): CpuFormat => load

  return {
    getLoad,
    setup,
    newLoads
  }
}

export default CalculateLoad()

/* draft.main.current = newData.main.current
draft.main.min = newData.main.current < temp.main.min ? newData.main.current : temp.main.min
draft.main.max = newData.main.current > temp.main.max ? newData.main.current : temp.main.max
draft.cores = CpuFormats.compareValues([...temp.cores], [...newData.cores]) */
