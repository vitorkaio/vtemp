import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    font: 16px Roboto, sans-serif;
    background-color: #fff;
    -webkit-font-smoothing: antialiased;
  }
  @font-face {
    font-family: "JetBrains Mono Medium";
    src: url("./fonts/jetbrains/JetBrainsMono-Medium.ttf");
  }
`
