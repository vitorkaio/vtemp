import React from 'react'
import { StateProps, DispatchProps } from './CpuTempTypes'

import { Container, Content, Main, AvgTemp, MinMaxTemp, Cores, Core } from './CpuTempStyles'
import ProgressCpu from '../progressCpu/ProgressCpu'
import { CpuFormat } from '../../services/CpuFormats'
import CalculateTemp from './CalculateTemp'

import { cpuTempGood, cpuTempMin, cpuTempWarning, cpuTempCritical } from '../../styles/Colors'
import { MyBadge, MyCore } from '../badges/Badges'
import Header from '../header/Header'
import Tooltip from '@material-ui/core/Tooltip'
import { CPUTooltip } from '../tooltips/Tooltips'

type Props = DispatchProps & StateProps

const CpuTemp: React.FC<Props> = ({ cpuTemp }) => {
  const newTemp: CpuFormat = CalculateTemp.newTemps(CalculateTemp.setup(cpuTemp))

  const color = (temp: number) => {
    const value = temp
    if (value <= 30) {
      return cpuTempMin
    } else if (value > 30 && value <= 50) {
      return cpuTempGood
    } else if (value > 50 && value <= 70) {
      return cpuTempWarning
    } else {
      return cpuTempCritical
    }
  }

  return (
    <Container>
      <Header title="CPU Temperature" />
      <Content>
        <Main>
          <AvgTemp>
            <Tooltip title={CPUTooltip.CPU_TEMP_CURRENT} aria-label="info">
              <div>
                <ProgressCpu
                  value={newTemp?.main.current}
                  text={`${newTemp?.main.current}°C`}
                  color={color(newTemp?.main.current)}
                  size={150} />
              </div>
            </Tooltip>
          </AvgTemp>
          <MinMaxTemp>
            <Tooltip title={CPUTooltip.CPU_TEMP_MIN} aria-label="info">
              <div>
                <ProgressCpu
                  value={newTemp?.main.min}
                  text={`${newTemp?.main.min}°C`}
                  color={cpuTempMin}
                  size={90} />
              </div>
            </Tooltip>
            <Tooltip title={CPUTooltip.CPU_TEMP_MAX} aria-label="info">
              <div>
                <ProgressCpu
                  value={newTemp?.main.max}
                  text={`${newTemp?.main.max}°C`}
                  color={cpuTempCritical}
                  size={90} />
              </div>
            </Tooltip>

          </MinMaxTemp>
        </Main>
        <Cores>
          {
            newTemp.cores.map((item, index) => (
              <Core key={index}>
                <MyCore><span>Core #{++index}</span></MyCore>
                <Tooltip title={CPUTooltip.CORE_TEMP_CURRENT} aria-label="info">
                  <MyBadge color={color(item.current)}><span>{item.current}</span></MyBadge>
                </Tooltip>
                <Tooltip title={CPUTooltip.CORE_TEMP_MIN} aria-label="info">
                  <MyBadge color={cpuTempMin}><span>{item.min}</span></MyBadge>
                </Tooltip>
                <Tooltip title={CPUTooltip.CORE_TEMP_MAX} aria-label="info">
                  <MyBadge color={cpuTempCritical}><span>{item.max}</span></MyBadge>
                </Tooltip>
              </Core>
            ))
          }
        </Cores>
      </Content>
    </Container>
  )
}

export default CpuTemp
