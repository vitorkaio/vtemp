## VTemp
App Electron for verify the temperature and load of processor.

![alt text](assets/vtemp.png)


## Installation

Use a package manager of your choice (npm, yarn, etc.) in order to install all dependencies

```bash
yarn install
```

## Usage
Run this project script will need to be executed `dev`

```bash
yarn dev
```

## Packaging
To generate a project package run `package`. Executable is generate in **release** folder

```bash
yarn package
```

Obs.: On Linux, will generated appImage

[MIT](https://choosealicense.com/licenses/mit/)
