import { CpuLoad } from '../../services/Cpu'

export interface StateProps {
  cpuLoad: CpuLoad
}

export interface DispatchProps {
  dispatch?(): void
}
