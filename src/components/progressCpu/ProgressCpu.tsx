import React from 'react'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'

import { Container } from './ProgressCpuStyles'
import { StateProps, DispatchProps } from './ProgressCpuTypes'

type Props = StateProps & DispatchProps

const ProgressCpu: React.FC<Props> = ({ value, text, size, color }) => {
  return (
    <Container size={size}>
      <CircularProgressbar
        value={value}
        text={`${text}`}
        styles={{
          // Colors
          path: {
            // Path color
            stroke: color
          },
          text: {
            fill: color,
            fontFamily: 'JetBrains Mono Medium'
          }
        }}
      />
    </Container>

  )
}

export default ProgressCpu
