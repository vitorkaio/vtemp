import styled from 'styled-components'
import { primaryColor } from '../../styles/Colors'

export const Container = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 0.5rem 0.5rem 0 0.5rem;
`

export const Title = styled.span`
  color: ${primaryColor[500]};
  font-size: 1.2rem;
  font-weight: bold;
  margin-left: 1rem;
`
