import React from 'react'

import { Container, Title } from './HeaderStyles'
import { StateProps, DispatchProps } from './HeaderTypes'

import Divider from '@material-ui/core/Divider'

type Props = StateProps & DispatchProps

const Header: React.FC<Props> = ({ title }) => {
  return (
    <Container>
      <Title>{title}</Title>
      <Divider style={{ flex: 1, marginLeft: '1rem', marginRight: '0.5rem' }}/>
    </Container>
  )
}

export default Header
