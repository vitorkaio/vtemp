import React from 'react'
import { StateProps, DispatchProps } from './CpuInfoTypes'

import { Container, Content } from './CpuInfoStyles'
import Header from '../header/Header'
import { MyCore as MyBadge } from '../badges/Badges'

type Props = DispatchProps & StateProps

const CpuInfo: React.FC<Props> = ({ cpuData }) => {
  return (
    <Container>
      <Header title="CPU Information" />
      <Content>
        <MyBadge><span>Brand: { cpuData?.brand }</span></MyBadge>
        <MyBadge><span>Family: { cpuData?.family }</span></MyBadge>
        <MyBadge><span>Vendor: { cpuData?.vendor }</span></MyBadge>
        <MyBadge><span>Cores: { cpuData?.physicalCores }</span></MyBadge>
        <MyBadge><span>Threads: { cpuData?.cores }</span></MyBadge>
      </Content>
    </Container>
  )
}

// Com o "memo" o component será renderizado apenas uma vez
export default React.memo(CpuInfo, () => { return true })
