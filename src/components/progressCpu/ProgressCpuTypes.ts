export interface StateProps {
  value: number,
  text: string,
  size: number,
  color: string
}

export interface DispatchProps {
  dispatch?(): void
}
